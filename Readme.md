# Yellowdig Viz #
A companion visualization app for the Yellowdig social pinboard

[Click to access (Subversion) source code repository, including detailed docs](https://source.at.northwestern.edu/svn/os/yellowdigViz)

## Important Note ##
 **Consultation with Yellowdig is recommended as a first step in considering use of this Yellowdig Viz code.** This (experimental) application depends on direct data transfers of Yellowdig event data from Yellowdig to Northwestern's AWS resources, and the particulars of this implementation may change in the future. You will need to establish an agreement and method with Yellowdig to obtain your institution's Yellowdig data.

## Resources required to run this project ##

If you choose to run the project without modification, in addition to hosting the Yellowdig Viz web application in a servlet container, you will need to provide a set of Amazon Web Service resources that are used (as a back-end) to process the Yellowdig event data in a continuous fashion. Please see the Subversion README file for detailed documentation about all the resources required for the project. 

## App Overview ##

Yellowdig Viz provides an instructor and his/her students a useful way of visualizing class interactions in the Yellowdig Discussion Boards. The tool uses a network graph to distinctively display the students, their Yellowdig pins (new posts referring to externally published content), and comments made on those pins or on other comments. The graph displays people and pin images when provided, as well as the specific text used within the Yellowdig interactions. Sizes of nodes in the graph reflect the popularity of particular pins or comments.

This tool makes it easy to focus attention on particular subsets of the graph that are of interest, by specifying both the type of 'entity' of interest, and also the time period in which the activity took place. The choice of entity can be made by directly interacting with the graph (e.g. clicking) or by selecting one or more entities listed in a right-hand side menu; while the choice of time period is determined by the state of a set of toggle buttons representing each week within a calendar term.

Students can choose to focus on entities such as people, pins and Yellowdig tags, while instructors can, in addition, -and assuming the optional text services feature is enabled- focus on an additional set of entities computed by the tool via the application of natural language processing services to the text used in the interactions themselves (that is within pins, comments and even external articles). Focusing on a pin restricts the view to the conversation thread defined by the pin; focusing on a person restricts the view to all of the person's activity; focusing on a Yellowdig tag restricts the view to all pins/comments which are indexed on the particular Yellowdig tag.

The App is LTI 1.0-compliant, and is installed in Northwestern University's Canvas LMS instance.

Apache 2.0 License. See the LICENSE.txt file in the project Subversion repository.